FROM python:3.6
RUN pip install Flask Flask-restful flask-jsonpify pandas flask-table
RUN pip install requests lxml SPARQLWrapper pyyaml ipinfo rdflib
RUN useradd -ms /bin/bash admin
USER admin
WORKDIR /fimypro
COPY flask_server /fimypro/flask_server
COPY data /fimypro/data
COPY config.txt /fimypro
WORKDIR /fimypro/flask_server
CMD ["python", "FiMyPro.py"]
