from flask import Flask, render_template, request,  jsonify
from source import html_table
from source import product_collector
from source import osm_search
from source import chefkoch_recipe
import requests

app = Flask(__name__)
@app.route('/',methods=['GET','POST'])



def index():

    #get client ipAddress
    if request.method == 'GET':
        #only works with GET hhtp request
        user_ip = request.remote_addr

    JS_message = ""
    if request.method == 'POST':
        #is Button on main Page
        if request.form.get("searchButton") == "Suche" and request.form.get("inputSearchRequest") != '':
            requestText = request.form.get("inputSearchRequest")
            isPlasticFilter = request.form.get("isPlastic")
            brandFilterStatus = request.form.get("inExclude")
            requestType = request.form.get("isRecipe")
            brandFilterText = request.form.get("branchFilter")
            ingredientFilter = ''
            if request.form.get("ingredientFilter") != '':
                ingredientFilter = request.form.get("ingredientFilter")
            else:
                ingredientFilter = "no Filter"
            prodList = []

            if requestType == "NE":
                #only one product was searched
                prodList = product_collector.product_list(requestText,ingredientFilter,isPlasticFilter,brandFilterText,brandFilterStatus)
                #prepare Table row list:
                tableRows = []
                productInformations = prodList.getProductList()
                for product in productInformations:
                    row = html_table.Item(product['Name'],product['Marke'],product['Preis_e'],product['Mass'],product['Verpackung'],product['Discounter'],product['Nutri'],product['Quelle'],product['Zutaten'],)
                    tableRows.append(row)
                # create html table from data
                table = html_table.ItemTable(tableRows)
            else:
                #a chefkoch recipe was searched
                try:
                    recipe = chefkoch_recipe.chefkoch_req(requestText)
                except:
                    return render_template('index.html',message=JS_message)
                recipeIngredients = recipe.get_ingredients()
                #prodList a instance with complex Search request --> determine faster
                #only need the instance not a result
                prodList = product_collector.product_list("asdheux",ingredientFilter,isPlasticFilter,brandFilterText,brandFilterStatus)
                #not so much Product like the pure Product Search
                prodList.set_productMax(10)
                #search products for all ingredients
                recipeProducts = []
                for ingredient in recipeIngredients:
                    prodList.clear_Prodlist()

                    #if it's not the first line add a free row
                    if ingredient != recipeIngredients[0]:
                        prodList.add_empty_row()
                    #add text from ingredient
                    prodList.add_recipeText(ingredient)
                    #add empty --> you can see the ingredient better
                    prodList.add_empty_row()
                    #have to add because createProductList overwrites
                    recipeProducts = recipeProducts + prodList.getProductList()
                    prodList.clear_Prodlist()
                    #set the Request Text new
                    prodList.set_productRequest(recipe.getIngredientRequestWord(ingredient))
                    #search for the products
                    prodList.createProductList()
                    recipeProducts = recipeProducts + prodList.getProductList()
                tableRows = []
                for product in recipeProducts:
                    row = html_table.Item(product['Name'],product['Marke'],product['Preis_e'],product['Mass'],product['Verpackung'],product['Discounter'],product['Nutri'],product['Quelle'],product['Zutaten'],)
                    tableRows.append(row)
                # create html table from data
                table = html_table.ItemTable(tableRows)
            #print(table.__html__())

            return render_template('results.html',table=table)
        #is the back Button on result page
        else:
            return render_template('index.html',message=JS_message)

    #"finden" was clicked
    if 'id' in request.args:
        if request.args['id'] != "-" and request.args['id'] != "":

            #load Openstreetmaps
            osm_api = osm_search.osm_search()

            #get Pos/city of the User
            position_user = osm_api.get_location(user_ip)
            #get Pos of the Store
            success = osm_api.get_coords_of_store(request.args['id'],position_user)
            if success == 1: #store was not found on in the city with Open STreet Maps
                JS_message = "true"
                render_template('index.html',message=JS_message)
            else:
                #load OSM Map at lat and lon
                lat = osm_api.get_lat()
                lon = osm_api.get_lon()
                return render_template("map.html",lat=lat,lon=lon)

    return render_template('index.html',message=JS_message)

if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000)
