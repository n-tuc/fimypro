import json
import requests
import ipinfo
class osm_search():

    #get the lat and lon of one store in the city
    #city = Area of the user
    #store = clicked link --> store of the product
    def get_coords_of_store(self,storeRequest,city):
        self.lat = ""
        self.lon = ""
        OSMrequestURL = "https://nominatim.openstreetmap.org/search?q="+storeRequest+"+"+city+"&format=json&polygon=0&addressdetails=1"
        OSM_API_result = requests.get(OSMrequestURL)
        for place in OSM_API_result.json():
            if storeRequest.lower() in place["display_name"].lower():
                self.lat = place["lat"]
                self.lon = place["lon"]
                return 0 #a store was found

        return 1


    def get_location(self,ipAddress):
        #use https://ipinfo.io
        access_token = '9a267ec834c113' #only 50000 requests per month
        handler = ipinfo.getHandler(access_token)
        ip_pos_details = handler.getDetails(ipAddress)
        ip_pos_details = handler.getDetails()
        return ip_pos_details.city

    def get_lat(self):
        return self.lat

    def get_lon(self):
        return self.lon