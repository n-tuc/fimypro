import json
import requests
import difflib
import operator
from source import stardog_requestor

class product_list():
    def __init__(self, request, ingredientFilter, isPlasticFilter, brandFilterText, brandFilterStatus):
        self.off_req = stardog_requestor.stardog_off()
        self.request = request
        self.productMax = 20
        self.productList = []
        self.ingredientFilter = ingredientFilter
        self.isPlasticFilter = isPlasticFilter
        self.brandFilterText = brandFilterText
        self.brandFilterStatus = brandFilterStatus
        self.createProductList()
    def createProductList(self):
        #with a brand Filter Request --> a other Sparql request
        if self.brandFilterText != "":
            self.off_req.requestWithBrandFilter(self.request,self.ingredientFilter,self.isPlasticFilter,self.brandFilterStatus,self.brandFilterText)
        else:
            self.off_req.normal_request(self.request,self.ingredientFilter,self.isPlasticFilter)
        self.productList = self.off_req.get_Products()
        if len(self.productList) < self.productMax:
            numMissingProducts = self.productMax - len(self.productList)
            self.addDataFromDPV(numMissingProducts)
        else:
            #to many entries
            self.cut_results_to_productMax()
    def addDataFromDPV(self, number_of_Products):
        requestURL = "http://webengineering.ins.hs-anhalt.de:32232/products/all,"+ str(number_of_Products) +"," + str(self.request)
        r = requests.get(requestURL)
        if r.json() != "no Match":
            for product_name in r.json():
                #add name
                product_values = r.json()[product_name]
                product_values['Name'] = product_name
                #change Preis ct to €
                product_values['Preis_e'] = product_values['Preis_ct'][0:-3] + "." + product_values['Preis_ct'][-3:len(product_values['Preis_ct'])] + "€"
                #add source
                product_values['Quelle'] = "Discounter Preisvergleich"
                #fill the OFF information fields
                product_values['Nutri'] = "-"
                product_values['Verpackung'] = "-"
                if product_values['Marke'] == "":
                    product_values['Marke'] = "-"
                product_values['Zutaten'] = "nein"
                product_values['request_similarity'] = difflib.SequenceMatcher(a=product_name.lower(), b=self.request.lower()).ratio()
                #brand filter
                if self.brandFilterStatus == "NE":
                    if product_values['Marke'] != self.brandFilterText:
                        self.productList.append(product_values)
                    else:
                        pass
                else:
                    if product_values['Marke'] == self.brandFilterText:
                        self.productList.append(product_values)
                    else:
                        pass

        #change order of the products --> best Request Similarity is the first Product
        self.productList = sorted(self.productList,key=operator.itemgetter("request_similarity"), reverse=True)

    #only use the best Entries until productMax reach
    def cut_results_to_productMax(self):
        newResults = []
        i=0
        for entry in self.productList:
            i = i + 1
            newResults.append(entry)
            if i > self.productMax:
                break
        self.productList = newResults

    def set_productMax(self,numberOfProds):
        self.productMax = numberOfProds
    def add_empty_row(self):
        row_data = {}
        row_data["Name"] = ""
        row_data["Quelle"] = ""
        row_data['Marke']= ""
        row_data['Preis_e'] =""
        row_data['Mass'] = ""
        row_data['Verpackung']=""
        row_data['Discounter']=""
        row_data['Nutri']=""
        row_data['Zutaten']=""
        self.productList.append(row_data)
    def add_recipeText(self,recipeText):
        #concat the string array
        recipeTextStr = ""
        for word in recipeText:
            if word != "no_unit" and word != "no" and word != "unit":
                recipeTextStr = recipeTextStr + " " + word
        row_data = {}
        row_data["Name"] = recipeTextStr
        row_data["Quelle"] = "Chefkoch Zutat"
        row_data['Marke']= ""
        row_data['Preis_e'] =""
        row_data['Mass'] = ""
        row_data['Verpackung']=""
        row_data['Discounter']=""
        row_data['Nutri']=""
        row_data['Zutaten']=""
        self.productList.append(row_data)
    def clear_Prodlist(self):
        self.productList = []
    def set_productRequest(self,newRequestText):
        self.request = newRequestText
    def getProductList(self):
        return self.productList