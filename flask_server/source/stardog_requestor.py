from SPARQLWrapper import SPARQLWrapper, JSON
import json
import requests
import difflib
from multiprocessing.dummy import Pool as ThreadPool
from functools import partial

class stardog_off():
    def __init__(self):
        #load config
        with open('../config.txt') as f:
            d = json.load(f)
            sparqlEndpoint = d["OFF_Sparql_endpoint"]
            user = d["endpoint_user"]
            password = d["endpoint_pw"]
            self.sparqlWrapper = SPARQLWrapper(sparqlEndpoint)
            self.sparqlWrapper.setCredentials(user,password)
            self.OffProducts = []
        #load plastic tags --> blacklist of the Packagingtags if the plastic filter is activated
        with open('../data/plasticTags.json') as f2:
            self.plasticTags = json.load(f2)["tags"]

    def normal_request(self,request,ingredientFilter,isPlasticFilter):
        self.OffProducts = []
        requestText = "'" + request + "'"#ready for stardog
        select_rq = """

         PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
           PREFIX food: <http://data.lirmm.fr/ontologies/food#>


            SELECT *
               WHERE {
                  ?about food:name ?name.
                  ?about food:code ?id
                  FILTER regex(?name, """ + requestText + """, "i").
               }
        """

        self.sparqlWrapper.setQuery(select_rq)
        self.sparqlWrapper.setReturnFormat(JSON)

        data_json = self.sparqlWrapper.query().convert()
        #call multithreading
        self.parallelRequests(data_json["results"]["bindings"],request,ingredientFilter,isPlasticFilter,10)
    #get the information parallel --> better performance
    #ListOfProducts = Array to call the products in different threads
    #other parameters are constant
    def parallelRequests(self,ListOfProducts,request,ingredientFilter, isPlasticFilter, threads):
        pool = ThreadPool(threads)
        #calls method to add Sparql request into the Table Structure
        pool.map(partial(self.create_row, request=request, ingredientFilter=ingredientFilter, isPlasticFilter=isPlasticFilter ), ListOfProducts)
        pool.close()
        pool.join()

    #get the packaging, Ingredients, nutri score, brand and store information
    #filter with string similarity if more than 11 matchs to the request text
    #use ingredient filter and use packaging filter
    def create_row(self,product,request,ingredientFilter,isPlasticFilter):
        row_data = {}
        row_data["Name"] = product["name"]["value"]

        row_data["request_similarity"] = difflib.SequenceMatcher(a=product["name"]["value"].lower(), b=request.lower()).ratio()
        #only the Productnames with a high similarity become a result
        if( (row_data["request_similarity"] >= 0.8 )or (len(product["name"]["value"]) < 12)):
            prodId = product["id"]["value"]
            row_data["Verpackung"] = self.get_info(prodId,"packaging")
            isPlastic = False #can only be true if the plastic filter is activated
            if isPlasticFilter:
                isPlastic = self.check_isPlasticTag(row_data["Verpackung"])
            row_data["Preis_e"] = "-"
            row_data["Quelle"] = "Openfoodfacts"
            row_data["Mass"] = "-"
            row_data["ZutatenListe"] = self.get_info(prodId,"ingredients_text")
            if row_data["ZutatenListe"] == "-":
                row_data["Zutaten"] = "nein"
            else:
                row_data["Zutaten"] = "ja"
            #check the forbidden ingredient
            if ingredientFilter in row_data["ZutatenListe"] or isPlastic:
                #dont add the Product
                pass
            else:
                row_data["Nutri"] = self.get_info(prodId,"nutrition_grades")
                row_data["Marke"] = self.get_info(prodId,"brands")
                row_data["Discounter"] = self.get_info(prodId,"stores")
                self.OffProducts.append(row_data)
        else:
            pass
    #check if a plastic Tag inside the packaging Tags
    #return "true" if plastic inside
    def check_isPlasticTag(self,packagingInfo):
        tags = packagingInfo.split(',')
        for tag in tags:
            if tag in self.plasticTags:
                return True

        #not plastic tag found
        return False
    def get_info(self,productId,relation):
        productId = "'" + productId + "'"#ready for SPARQL
        sparqlRel = "food:" + relation
        select_rq = """

         PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
           PREFIX food: <http://data.lirmm.fr/ontologies/food#>


            SELECT *
               WHERE {
                  ?about food:code """ + productId + """.
                  ?about """+ sparqlRel + """ ?info
               }
        """
        self.sparqlWrapper.setQuery(select_rq)
        self.sparqlWrapper.setReturnFormat(JSON)
        data_json = self.sparqlWrapper.query().convert()
        infoTags = ""
        for product_info in data_json["results"]["bindings"]:
            #dont add "," at the first time
            if infoTags != "":
                infoTags = infoTags + ","
            infoTags = infoTags + product_info["info"]["value"]
        if infoTags == "":
            infoTags = "-"
        return infoTags

    def requestWithBrandFilter(self,request,ingredientFilter,isPlasticFilter,brandFilterStatus,brandRequest):
        self.OffProducts = []
        if brandFilterStatus == "NE":
            compareType = "!="
        else:
            compareType = "="
        brandRequest = "'"+brandRequest+"'"
        requestText = "'" + request + "'"#ready for stardog
        select_rq = """

         PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
           PREFIX food: <http://data.lirmm.fr/ontologies/food#>


            SELECT *
               WHERE {
                  ?about food:name ?name.
                  ?about food:code ?id.
                  ?about food:brands ?brand.
                  FILTER (?brand """ + compareType +""" """+ brandRequest + """).
                  FILTER regex(?name, """ + requestText + """, "i").
               }
        """

        self.sparqlWrapper.setQuery(select_rq)
        self.sparqlWrapper.setReturnFormat(JSON)

        data_json = self.sparqlWrapper.query().convert()
        self.parallelRequests(data_json["results"]["bindings"],request,ingredientFilter,isPlasticFilter,10)
    def get_Products(self):
        return self.OffProducts