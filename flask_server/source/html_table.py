# import things
from flask_table import Table, Col, ButtonCol, LinkCol
import json

# Declare your table
class ItemTable(Table):
    classes = ["u-max-full-width"]
    name = Col('Name')
    brand = Col('Marke')
    mass = Col('Maßangabe')
    price = Col('ca. Preis')
    packaging = Col('Verpackung')
    nutri = Col('Nutri Score')
    ingredients = Col('Zutaten-Informationen vorhanden?')
    source = Col('Quelle')
    discounter = Col('Geschäft')
    goToDiscounter = LinkCol('', 'index',url_kwargs=dict(id='discounter'), text_fallback="finden")
    empty = Col(' ') #only for looking beautiful


# Get some objects
class Item(object):
    def __init__(self, name, brand, price, mass, packaging,discounter, nutri, source, ingredients):
        self.name = name
        self.brand = brand
        self.price = price
        self.mass = mass
        self.discounter = discounter
        self.nutri = nutri
        self.source = source
        self.ingredients = ingredients
        self.packaging = packaging
        self.empty = " "

