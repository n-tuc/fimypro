from rdflib import Graph
import json

class chefkoch_req():
    #get one recipe with an ingredients list
    #then cook with them
    #cooking is a CREATIVE Hobby you don't need instructions!
    def __init__(self,recipeRequest):
        #delete ä ö ü for the chefkoch request
        recipeRequest = recipeRequest.replace("ä","ae")
        recipeRequest = recipeRequest.replace("ö","oe")
        recipeRequest = recipeRequest.replace("ü","ue")
        g = Graph()
        try:
            g.load("http://any23-vm2.apache.org/rdf/https://www.chefkoch.de/rs/s0/"+ recipeRequest +"/Rezepte.html")
        except:
            return none
        #always use the first one
        recipe_list = g.query(
    """
   PREFIX sm: <http://schema.org/>

    SELECT ?name ?recipe ?url
       WHERE {
          ?recipe sm:name ?name.
          ?recipe sm:url ?url
       }
       LIMIT 1
         """ )
        for item in recipe_list:
            url_of_recipe = "http://any23-vm2.apache.org/rdf/" + str(item[2])
        self.recipe = Graph()
        self.recipe.load(url_of_recipe)
        for s, p, o in self.recipe:
            print(s,p,o)

    def get_ingredients(self):

        ingredients_query = self.recipe.query(
    """
   PREFIX sm: <http://schema.org/>

    SELECT ?recipe ?ingredients
       WHERE {
          ?recipe sm:recipeIngredient ?ingredients.
       }
         """
)
        ingredient_list = []
        for ingredient in ingredients_query:
            words = str(ingredient[1]).split()
            #delete "," in array --> no words
            if "," in words:
                words.remove(",")

            splitted_info = []
            print(words)
            for i in range(len(words)):
                UndorOderPos = 0


                #the first word or after a Und/Oder
                if(i == 0 or words[i].lower() == "und" or words[i].lower() == "oder" ):
                    if words[i].lower() == "und" or words[i].lower() == "oder":
                        UndorOderPos = i+1
                        #gets their own tuple because different products
                        if words[i].lower() == "und":
                            ingredient_list.append(splitted_info)
                            splitted_info = []
                    #checks if is a digit is in the ingredient
                    digit_check = words[0 + UndorOderPos].replace(",","")
                    if digit_check.isdigit():
                        splitted_info.append(words[0 + UndorOderPos])
                        #the word after the number is the unit or the ingredient if only 2 words
                        splitted_info.append(words[1 + UndorOderPos])
                        try:
                            #search for the first upper word --> no adjective
                            for search_noun in range(UndorOderPos, len(words)):
                                withoutBrackets = words[2+search_noun].replace("(","")
                                withoutBrackets = withoutBrackets.replace(")","")
                                if search_noun == len(words) - 3:
                                    splitted_info.append(words[2 + UndorOderPos])
                                    break
                                if withoutBrackets.istitle():
                                    splitted_info.append(withoutBrackets)
                                    break


                        except IndexError: #only one word after the number --> no unit
                            splitted_info[1] = "no_unit"
                            splitted_info.append(words[1 + UndorOderPos])
                    else:
                        splitted_info.append("no")
                        splitted_info.append("unit")
                         #search for the first upper word --> no adjective

                        for search_noun in range(UndorOderPos, len(words)):
                            #replace Brackets that istitle works
                            withoutBrackets = words[search_noun].replace("(","")
                            withoutBrackets = withoutBrackets.replace(")","")
                            if search_noun == len(words):
                                splitted_info.append(words[UndorOderPos])
                                break
                            if withoutBrackets.istitle():
                                splitted_info.append(withoutBrackets)
                                break


            ingredient_list.append(splitted_info)
        return ingredient_list

    #find out the correct request word of a ingredient information
    def getIngredientRequestWord(self,ingredientTextArray):

        if len(ingredientTextArray) > 2:
            return ingredientTextArray[2]
        elif len(ingredientTextArray) > 1:
            return ingredientTextArray[1]
        else:
            return ingredientTextArray[0]

                          