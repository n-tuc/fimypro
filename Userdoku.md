# Nutzer Anleitung

FiMyPro ist eine  Webanwendung, die es ermöglicht nach Lebenmitteln zu suchen.
Alle Ergebnisse besitzen keine Garantie auf Richtigkeit.

*Zugriff zur Zeit möglich:*
```
http://webengineering.ins.hs-anhalt.de:32235
```

## Verwendung der Suchmaske auf der Startseite
Wenn man sich auf der Startseite der Anwendung befindet, sieht man eine Suchmaske. 
Über dem großen Sucheingabefeld gibt es zwei Radiobutton. Man kann mit diesen Button, zwischen den zwei verschiedenen Modi der Anwendung wechseln.
Wenn der Button auf "Produkt" steht, ist der Modus "Produktsuche" aktiviert. Wenn der Button auf "Zutatenliste eines Rezepts von Chefkoch" steht ist, der Modus "Chefkoch Suche" aktiviert.
Beide Modi werden im Folgenden erklärt.

### Modus "Produkt Suche"
Wenn sich die Anwendung in diesem Modus befindet, kann man nach einem Produkt zu einem Lebensmittel suchen. Man kann das gewünschte Lebensmittel im Textfeld über den "Suche"-Button angeben.
Danach muss man den "Suche"-Button betätigen.
Dadurch gelangt man zu der Ergebnis-Anzeige. Diese Anzeige zeigt eine Liste mit gefundenen Produkten an, die auf die vorher angegebenen Filter passen. Die Ergebnis-Anzeige und die Filtermöglichkeiten werden später erklärt.

### Modus "Chefkoch Suche"

Wenn man die Anwendung auf diesen Modus gestellt hat, sucht man mit seiner Texteingabe im Suchfeld nach einem Rezept von [Chefkoch](https://www.chefkoch.de/). Dies ist allerdings keine normale Rezeptsuche sondern man erhält nur die Zutaten.
Es dient als Inspiration zum Kochen. Man muss sich anhand der Zutaten selber überlegen, wie man das Gericht kochen könnte. Damit ist es eher nicht für Kochanfänger geeignet sondern zielt auf Köche ab, die Inspiration und Abwechslung beim Kochen suchen.
Für diese Köche ist Suche gut geeignet, da man nicht immer das selbe Rezept erhält bei gleicher Suchanfrage und man schnell eine Übersicht an Zutaten und die dazugehörigen Produkten erhält.

Auch in diesem Modus gelangt man über den "Suche"-Button in den Ergebnis-Anzeige. Als Ergebnisliste erhält man in diesem Modus immer eine Zutat gefolgt von 10 Produktvorschlägen. Auch in diesem Modus werden nur Produkte angezeigt, die auf den Filter passen.

## Filtermöglichkeiten

### ohne Plastikverpackung
Wenn diese Checkbox einen Haken hat, ist der Filter aktiviert. In diesem Fall werden keine Produkte ausgegeben, die in Plastik verpackt sind. 
Dieser Filter funktioniert nur, wenn auch Informationen über die Verpackung vorhanden sind. Dies kann man in der Ergebnisliste überprüfen. Wenn in der Spalte "Verpackung" kein Text steht, ist die Information nicht vorhanden.

### Welcher Inhaltsstoff ist verboten?
In dieses Textfeld kann man einen Inhaltsstoff angeben, der in den Produkten nicht enthalten sein soll. Ob dieser Filter funktioniert kann man in Punkt "Zutaten-Informationen vorhanden?". Wenn dort ein "nein" steht
funktioniert der Zutatenfilter bei diesem Produkt nicht. Allerdings ist in manchen Fällen auch die Zutatenliste der Produkte nicht vollständig.

### Markenfilter
Dieser Filter besitzt zwei Modi. Es gibt die Möglichkeit nach einer bestimmten Marke zu suchen oder eine bestimmte Marke auszuschließen. 
Die Modi kann man mit den Radiobutton "ausschließen" und "suchen" wechseln.
Im Textfeld des Filters kann man die Marke angeben, nach der man filtern möchte. Hierbei ist auf Groß- und Kleinschreibung zu achten, da sonst der Filter nicht funktioniert.
Der Filter liefert bei der Suche nach einer Marke direkt, eine Liste von Produkten dessen Marke bekannt ist. Produkte, wo nicht die Information vorhanden ist, werden nicht ausgegeben.
Wenn man eine Marke ausschließt funktioniert der Filter nicht auf den Produkten, die in der Spalte "Marke" den Wert "-" stehen haben, da bei diesen Produkten die Information zur Marke nicht vorhanden ist.

## Ergebnis-Anzeige
Diese Anzeige besteht aus einem "zurück"-Button und der Ergebnisliste. Wenn man den "zurück"-Button drückt kommt man zum Startbild zurück.

Die Ergebnisliste ist eine Tabelle, die alle gefundenen Produkte auflistet. Angezeigt Informationen sind: Name, Marke, Maßangabe, Preis, Verpackung, Nutri-Score, ob die Zutateninformation vorhanden ist, Quelle, Geschäft(wo man das Produkt kaufen kann).

In der Spalte Maßangabe können verschiedene Informationen angegeben sein wie z. B. das Volumen des Produkts oder das Gewicht des Produkts. Die Ausgabe ist nach Art des Produkts unterschiedlich.

Mit der Spalte Quelle ist gemeint, woher die Anwendung die Informationen holt. Die Produkt Informationen können von [OpenFoodFacts](https://world.openfoodfacts.org/) oder von [Discounter Preisvergleich](https://www.discounter-preisvergleich.de/) kommen.

Am Ende der Tabelle, ist ein Feld "finden" zu sehen. Wenn man auf diesen Link klickt, kommt man zu der Karten-Anzeige. 

## Karten-Anzeige

Die Karten-Anzeige wird über den Link "finden" in der Ergebnistabelle geöffnet. Sie öffnet sich nur, wenn in der Zeile die Information "Geschäft" gefüllt ist.
Im Kartenbildschirm wird eine Karte geladen und eine Filiale des Geschäftes, welches das Produkt verkauft, eingezeichnet. 

Auch in dieser Anzeige gelangt man über den "zurück"-Button wieder zum Startbildschirm.



