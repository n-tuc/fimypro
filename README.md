# FiMyPro - Find my product!

FiMyPro ist eine Webanwendung, die es ermöglicht, nach Lebensmitteln zu suchen,
die den eigenen Ansprüchen eines Produkt genügen. Man kann die Suche mit
verschiedenen Filtern versehen, um genau die Lebensmittelprodukte zu finden, die
man gerne möchte. Neben der Möglichkeit die Suche durch Filter einzuschränken,
erhält man auch zu den Produkten eine Reihe von weiteren Informationen.
Als zweiter Modus, kann man nach einem Rezept auf [Chefkoch](https://www.chefkoch.de/) suchen.
Dieser Modus ist für
Personen gedacht, die Inspirationen beim
Kochen brauchen, aber nicht zwingend ein
Rezept lesen wollen. Man gibt ein Stichwort
ein und erhält eine Liste der Zutaten und
jeweils 10 Vorschläge für passende Produkte.
Auch hier kann man die Produktsuche so
anpassen, dass man bloß die Produkte
vorgeschlagen bekommt, die den
gewünschten Vorstellungen genügen.

*Zugriff zur Zeit möglich:*
```
http://webengineering.ins.hs-anhalt.de:32235
```

## Deployment mit Docker
Man kann die Anwendung selber als Dockercontainer starten.
Einfach ein Dockerimage erstellen und den Container starten. Der Flask-Server läuft standardmäßig auf Port 5000.

### Testen der Komponenten
Vor der Erstellung des Docker Images sollte man zwei Komponenten testen ob sie noch erreichbar sind. 
\
**Stardog Server**
\
Voreingestellt ist der Stardog Server über die Adresse: http://webengineering.ins.hs-anhalt.de:32233/ zu erreichen. 
Die Einstellung eines eigenen Servers als Sparql-Endpoint wird im folgenden Punkt erklärt.
\
**Discounter Preisvergleich API**
\
Im Quellcode wird für Discounter Preisvergleich die Adresse: http://webengineering.ins.hs-anhalt.de:32232/ verwendet.
Wenn diese Adresse nicht erreichbar ist oder man seinen eigenen Server verwenden möchte, dann kann man seine eigene [Discounter Preisvergleich API](https://gitlab.com/n-tuc/discounter-preisvergleich-api) laufen lassen.
In diesen Fall muss man den Code an der Stelle: 
```
fimypro/flask_server/source/product_collector.py
```
in der Methode "addDataFromDPV" zu
```
 requestURL = "http://mein.server:port/products/all,"+ str(number_of_Products) +"," + str(self.request)
```
modifizieren.

**IP Lokalisierung (in Entwicklung)** 
\
Zum Finden eines Geschäfts in der Nähe, muss [Ipinfo.io](https://ipinfo.io/?token=9a267ec834c113) noch Ergebnisse liefern. Ohne diese Informationen kann der User nicht lokalisiert werden.

### Verwendung eines eignen Sparql-Endpoint
Wenn man seinen eigenen Sparql-Endpoint verwenden möchte findet man die verwendeten Daten an der Stelle:
```
fimypro/data/OFF_with_info.ttl
```
Anschließend kann man den Sparql-Endpoint sowie User-Name und User-Passwort in der Datei:
```
fimypro/config.txt
```
angeben.

## verwendete Daten

* [OpenStreetMaps](https://www.openstreetmap.de/) - Kartenmaterial
* [OpenFoodFacts](https://de.openfoodfacts.org/) - Lebensmittel-Datenbank
* [Discounter Preisvergleich](https://www.discounter-preisvergleich.de) -Produktsuche
* [Chefkoch](https://www.chefkoch.de/) -Rezepte

### Scripts zur Erstellung der Daten
Im Ordner "Scripts" lassen sich Python-Scripts finden. Diese dienen rein zur Generierung der Daten.
Zur Verwendung der Scripts benutzte ich einen lokalen Stardog-Server mit einer Datenbank mit den Daten von [OpenFoodFacts](https://de.openfoodfacts.org/data).
\
**delete_ng_entries.py** 
\
Dieser Script löscht alle Produkteinträge, die nicht in Deutschland verkauft werden.
\
**get_OFF_info.py**
\
Dieser Script ergänzt die RDF von Openfoodfacts mit der [API von OpenFoodFacts](https://de.openfoodfacts.org/data).
Hinzugefügte Informationen sind:

* Inhaltsstoffe
* Verpackung
* Geschäft, wo das Produkt angeboten wird
* Marke
* Nutri-Score
* Link zu einem Bild des Produkts

**get_package_types.py**
\
Dieser Script speichert eine Liste an verschiedenen Verpackungsmaterialien. Diese Liste dient als Grundlage für den Filter: "keine Plastikverpackung".
## Erstellt mit

* [Stardog](https://www.stardog.com/) - als Triplestore
* [Skeleton](http://getskeleton.com/) - als Frontend-Framework
* [Flask](https://palletsprojects.com/p/flask/) -als Webserver
* [Openlayers 2](https://openlayers.org/two/) -zur Einbindung von OpenStreetMaps
* [Ipinfo.io](https://ipinfo.io) -zur Lokalisierung einer Client-IP
* [any23](http://any23.apache.org/) zum Erhalt der Daten von Chefkoch im RDF-Format

## Entwickler

* **Ernst Stötzner** 
ernst.stoetzner@student.hs-anhalt.de



