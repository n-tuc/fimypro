from SPARQLWrapper import SPARQLWrapper, JSON
from multiprocessing.dummy import Pool as ThreadPool
import json
import requests

#Sparql endpoint of my Stardog triple store with Openfood facts RDF
endpoint = 'http://localhost:5820/myDB/query'
#product_name to check rdf is consistent with JSON Api
infoTags = ["ingredients_text","product_name","packaging","stores","nutrition_grades","ingredients_analysis_tags","image_front_url","brands"]

OFFData = SPARQLWrapper(endpoint)

# add your username and password if required
OFFData.setCredentials('PythonZugriff', 'passwort')

#Sparql wrapper to delete the Products outside the german Market
insertEndPoint = 'http://localhost:5820/myDB/update'
sparqlInsertEndPoint = SPARQLWrapper(insertEndPoint)
sparqlInsertEndPoint.setCredentials('PythonZugriff', 'passwort')
sparqlInsertEndPoint.setMethod("POST")

print("start collect Data")

#get all Products with IDs to call the Openfood facts JSON Api
request = """

 PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
   PREFIX food: <http://data.lirmm.fr/ontologies/food#>


    SELECT ?about ?name ?id
       WHERE {
          ?about food:name ?name.
          ?about food:code ?id
       }
"""

OFFData.setQuery(request)
OFFData.setReturnFormat(JSON)


OFFdata_json = OFFData.query().convert()



#add information to product
def insertProductInfo(ProductID,relation,value):
    ProductID = '"' + ProductID + '"' # "ID" <- Right form for the SPARQL Request
    relation = "food:" + relation
    value = value.replace("%","Prozent") #*not allowed for Sparql request
    value = value.replace("*","-")
    value = '"' + value + '"'
    insertQuery = """

 PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
   PREFIX food: <http://data.lirmm.fr/ontologies/food#>

 INSERT{?product """ + " " + relation + " " + value + """}
       WHERE {
          ?product food:code ?id.
          FILTER (?id = """ + ProductID + """)
          ?product ?p ?o.
       }
"""
    sparqlInsertEndPoint.setQuery(insertQuery)
    sparqlInsertEndPoint.query()

#calls the Openfoodfacts JSON API with the ProductID
#and fulfil the rdf with more information
def getProductInfo(ProductID):

    #Openfoodfacts API returns a JSON with more information of the product than the RDF of Openfoodfacts
    OFFrequestURL = "https://world.openfoodfacts.org/api/v0/product/" + ProductID + ".json"
    OFF_API = requests.get(OFFrequestURL)

    if(OFF_API.json()['status_verbose'] == 'product not found'):
        #Product has no more Information
        pass

    if 'product' in OFF_API.json():

        #Check all infoTags and add the information to the rdf
        for tag in infoTags:
            if tag in OFF_API.json()['product']:
                infos = []
                infos = OFF_API.json()['product'][tag]
                try:
                    infos = infos.split(",")
                except:
                    #its not a String
                    pass
                if len(infos) != 0:
                    for info in infos:
                        try:
                            insertProductInfo(ProductID,tag,info)
                        except:
                            print("error at:")
                            print(info)
def parallelRequests(ListOfIDs, threads):
    pool = ThreadPool(threads)
    pool.map(getProductInfo, ListOfIDs)
    pool.close()
    pool.join()


print("Select done")
print(len(OFFdata_json["results"]["bindings"]))
productIds = []

for product in OFFdata_json["results"]["bindings"]:
    productIds.append(product["id"]["value"])


parallelRequests(productIds, 10)
print("collect Data finished")
