from SPARQLWrapper import SPARQLWrapper, JSON
from multiprocessing.dummy import Pool as ThreadPool
import json
import requests

#Sparql endpoint of my Stardog triple store with Openfood facts RDF
endpoint = 'http://localhost:5820/myDB/query'

OFFData = SPARQLWrapper(endpoint)

# add your username and password if required
OFFData.setCredentials('PythonZugriff', 'passwort')


#get all package types
request = """

 PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
   PREFIX food: <http://data.lirmm.fr/ontologies/food#>


    SELECT Distinct ?packaging Where{
  ?s food:code ?code.
  ?s food:packaging ?packaging.
  }
"""

OFFData.setQuery(request)
OFFData.setReturnFormat(JSON)


OFFdata_json = OFFData.query().convert()

#add all values in a json file --> later the Packagingtags without plastic will delete per hand
packagingTags = []
for product in OFFdata_json["results"]["bindings"]:
    #filter key words out the packaging tags --> smaller file to search the plastic tags
    if not "tetra" in product["packaging"]["value"].lower() and "pappe" not in product["packaging"]["value"].lower() and "glas" not in product["packaging"]["value"].lower():
        packagingTags.append(product["packaging"]["value"])


with open('../data/packagingTags.json', 'w', encoding='utf-8') as f:
    json.dump(packagingTags, f, ensure_ascii=False, indent=4)
print("tags were saved")
