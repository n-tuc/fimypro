from SPARQLWrapper import SPARQLWrapper, JSON
from multiprocessing.dummy import Pool as ThreadPool
import json
import requests

#Sparql endpoint of my Stardog triple store with Openfood facts RDF
endpoint = 'http://localhost:5820/myDB/query'
countryTags = ["en:Germany","Germany","Deutschland"," Deutschland","DE","en:de"," en:de","en:DE"]

OFFData = SPARQLWrapper(endpoint)

# add your username and password if required
OFFData.setCredentials('PythonZugriff', 'passwort')

#Sparql wrapper to delete the Products outside the german Market
deleteEndPoint = 'http://localhost:5820/myDB/update'
sparqlDeleteEndPoint = SPARQLWrapper(deleteEndPoint)
sparqlDeleteEndPoint.setCredentials('PythonZugriff', 'passwort')
sparqlDeleteEndPoint.setMethod("POST")

print("start clean Data")

#get all Products with IDs to call the Openfood facts JSON Api
request = """

 PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
   PREFIX food: <http://data.lirmm.fr/ontologies/food#>


    SELECT ?about ?name ?id
       WHERE {
          ?about food:name ?name.
          ?about food:code ?id
       }
"""

OFFData.setQuery(request)
OFFData.setReturnFormat(JSON)


OFFdata_json = OFFData.query().convert()



#delete all information of a Product with the ID <- ProductID
def deleteProduct(ProductID):
    ProductID = '"' + ProductID + '"' # "ID" <- Right form for the SPARQL Request
    deleteQuery = """

 PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
   PREFIX food: <http://data.lirmm.fr/ontologies/food#>

 DELETE{?product ?p ?o}
       WHERE {
          ?product food:code ?id.
          FILTER (?id = """ + ProductID + """)
          ?product ?p ?o
       }
"""
    sparqlDeleteEndPoint.setQuery(deleteQuery)
    sparqlDeleteEndPoint.query()

#calls the Openfoodfacts JSON API with the ProductID
#and delete the Product if it has no German Tag in "Countries"
def getProductInfo(ProductID):

    #Openfoodfacts API returns a JSON with more information of the product than the RDF of Openfoodfacts
    OFFrequestURL = "https://world.openfoodfacts.org/api/v0/product/" + ProductID + ".json"
    OFF_API = requests.get(OFFrequestURL)

    if(OFF_API.json()['status_verbose'] == 'product not found'):
        #Product has no more Information than the name
        deleteProduct(ProductID)

    if 'product' in OFF_API.json():

        #Checks the Country information
        #--> delte Request if not German/Germany identified
        if 'countries' in OFF_API.json()['product']:
            countries = []
            countries = OFF_API.json()['product']["countries"]
            countries = countries.split(",")
            notInGermany = True
            for country in countries:
                if country in countryTags:
                    notInGermany = False
                    break
            if notInGermany:
                deleteProduct(ProductID)

def parallelRequests(ListOfIDs, threads):
    pool = ThreadPool(threads)
    pool.map(getProductInfo, ListOfIDs)
    pool.close()
    pool.join()


print("Select done")
print(len(OFFdata_json["results"]["bindings"]))
productIds = []

for product in OFFdata_json["results"]["bindings"]:
    productIds.append(product["id"]["value"])


parallelRequests(productIds, 10)
print("clean Data finished")